# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2019-12-18
### Added
- Protocol version negotiation
- PA message format description
### Changed
- Text message format

## [0.2.0] - 2019-12-17
### Added
- Compare links
- License

## 0.1.0 - 2019-12-17
### Added
- This changelog
- Basic description of human2pa protocol

[Unreleased]: https://gitlab.com/personal-assistant-bot/human2pa/protocol/compare/v0.3.0...master
[0.3.0]: https://gitlab.com/personal-assistant-bot/human2pa/protocol/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/personal-assistant-bot/human2pa/protocol/compare/v0.1.0...v0.2.0
