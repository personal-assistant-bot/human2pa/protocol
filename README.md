# human2pa protocol format

This document describes the protocol used to communicate with human2pa
translator service.

## Basics

The protocol is based on message exchange. Each message exchange is
initiated by client by sending a message to the service. The service
must reply with exactly one reply message. If client sends multiple
messages, the server sends replies in the same order.

Each message is a single sequence of bytes (which may include unicode
if both client and service support that) ending with a single `LF`
(`0x0A`) byte. Message cannot contain any other `LF` bytes.

All beginning and trailing whitespace bytes should be discarded.

Empty message (only whitespace characters) should be ignored (the
"exactly one" message sent by service means there is exactly one
non-empty message).

Message body must be a valid JSON.

## Message types

There are two basic message types that client can send to service:

- command
- text

### Protocol version

Both service providing human2pa/pa2human translation and the client
must be able to use the same version of protocol. Each communication
channel opened to the service is handled using some specific version
of the protocol and client must both adhere to it when sending
messages and correctly interpret the results. The protocol version can
be negotiated using the `version-get` and `version-set` commands.

#### `version-get`

The `version-get` command must have the following format:

    {"command": "version-get"}

(whitespaces inside the message are ignored)

The server must reply with the following message:

    {"versions": [<list-of-supported-versions>]}

where each element in `versions` list is a string specifying a version
or a range of versions.

#### `version-set`

The `version-set` command must have the following format:

    {"command": "version-set", "version": "<version-string>"}

where `version` value is the version is one of the versios supported
by the service.

The server will either reply with if it is able to switch to that
version

    {"result": "ok"}

or

    {"result": "error", "reason": "<reason>"}

otherwise. The `reason` string will be equal to `"not supported"` if
the version is not supported by the service. It may contain other
values specific to the service.

### Command

Command message is a command sent to service to perform some internal
actions. Commands are service-specific. Replies to commands are
service-specific.

Command message must contain `command` field containing the name of
command to be executed. It may contain additional data required for
that command.

### Text

Text message is a request to translate between human language and PA
format. Each message is a part of an ongoing chat. Text message must
contain (but not limited to) the following fields:

- `bot`
- `user`
- exactly one of `human` or `pa`

The `bot` field should specify the identifier of PA
instance/personality participating in the chat.

The `user` field should specify the identifier of human participating
in the chat.

If `human` field is present it should be either a string or a list of
strings to be translated to PA format.

If `pa` field is present it should be an object or a list of objects
containing messages in PA format.

Other fields may contain additional information required for
translation.

### Text reply

The reply message will contain the result of translation:

- for messages with `human` field it will contain `pa` field with
  parsed text (or multiple texts)
- for messages with `pa` field it will contain `human` field with
  generated text (or multiple text strings)

It is possible that number of elements in initial message and reply is
different.

Reply message may contain other fields.

### PA message format

Each PA message has the following format (whitespaces and line breaks
added for readability):

    {"intent": "<intent>",
     "data": {"parameter": "value",
              "other-parameter": ["value1", "value2"]},
     "extra": ["extra1", "extra2"]}

Each message must have exactly one `intent` field which specifies the
intent of the message. It may have `data` field which will be an
object containing additional information (named strings or lists of
strings). It may have `extra` field which will be a list of "extra"
information strings.
